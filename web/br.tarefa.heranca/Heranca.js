/**
 * Created by heitor on 06/09/15.
 */
/*Animal*/
function Animal(nome) {
    this.nome || 'animal';
};

var AnimalPrototipo = {
    fazerBarulho: function () {
        throw 'Deve ser implementado';
    }
};
/*Cao*/
function Cao(nome, raca) {
    Animal.call(this, nome)
    this.raca = raca || 'vira-lata';
};

var CaoPrototipo = new Animal();

CaoPrototipo.fazerBarulho = function () {
    return 'Au-Aau!';
};

/*Gato*/
function Gato(nome, raca) {
    Animal.call(this, nome)
    this.raca = raca || 'vira-lata';
};

var GatoPrototipo = new Animal();

GatoPrototipo.fazerBarulho = function () {
    return 'Miau!';
};
/*Manada*/
function Manada() {
    this.lista = [];
}

var ManadaPrototipo = {
    addAnimal: function (animal) {
        this.lista.push(animal);
    }
};

Manada.prototype = ManadaPrototipo;

/*Manada Virgula*/
function ManadaVirgula() {
    this.lista = [];
}
var ManadaVirgulaPrototipo = new Manada()
ManadaVirgulaPrototipo.fazerBarulho = function () {
    retorno = '';
    for (var i = 0; i < this.lista.length; i++) {
        retorno = retorno + this.lista[i].fazerBarulho() + ', ';
    }
    return retorno;
};
/*Manada Sustenido*/
function ManadaSustenido() {
    this.lista = [];
}
var ManadaSustenidoPrototipo = new Manada()
ManadaSustenidoPrototipo.fazerBarulho = function () {
     retorno = '';
    for (var i = 0; i < this.lista.length; i++) {
       retorno = retorno + this.lista[i].fazerBarulho() + '#';
    }
    return retorno;
};

/*Herança*/

CaoPrototipo.prototype = Animal.prototype;
Cao.prototype = CaoPrototipo;

GatoPrototipo.prototype = Animal.prototype;
Gato.prototype = GatoPrototipo;

ManadaVirgulaPrototipo.prototype = Manada.prototype;
ManadaVirgula.prototype = ManadaVirgulaPrototipo;

ManadaSustenidoPrototipo.prototype = Manada.prototype;
ManadaSustenido.prototype = ManadaSustenidoPrototipo;

/*Teste*/

var cao = new Cao("dog");
var gato = new Gato('cat')


/* Manada Virgula Teste*/
var manadaVirgula = new ManadaVirgula();
manadaVirgula.addAnimal(cao)
manadaVirgula.addAnimal(gato)
console.log(manadaVirgula.fazerBarulho());

/* Manada Sustenido Teste*/

var manadaSustenido = new ManadaSustenido();
manadaSustenido.addAnimal(cao)
manadaSustenido.addAnimal(gato)
console.log(manadaSustenido.fazerBarulho());


//end





