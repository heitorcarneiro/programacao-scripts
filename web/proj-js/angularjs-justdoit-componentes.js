/**
 * Created by heitor on 08/10/15.
 */
(function () {
    var categoriaComponents = angular.module('categoria-components', ['categoria-servicos']);
    /*section-register - > categoriaForm*/
    categoriaComponents.directive('sectionRegister', function () {
        return {
            restric: 'E',
            templateUrl: 'proj-html/section-register.html',
            replace: true,
            scope: {
                salvarCategoriaListener: '&'
            },
            controller: function ($scope, CategoriaAPI) {
                $scope.categoria = {title: 'QunitTest'};
                $scope.erros = {};
                $scope.salvandoCategoriaFlag = false;
                $scope.loading = false;

                $scope.salvar = function () {
                    $scope.erros = {};
                    $scope.salvandoCategoriaFlag = true;
                    $scope.loading = true;
                    CategoriaAPI.salvar($scope.categoria, function (categoriaSalva) {
                            $scope.salvarCategoriaListener({'categoria': categoriaSalva});
                            $scope.categoria = { };
                        },
                        function (erros) {
                            $scope.erros = erros;
                            console.log(erros);
                        }, function () {
                            $scope.salvandoCategoriaFlag = false;
                            $scope.loading = false;
                        });
                }

            }
        };
    });
    /* section-table -> categoriaTabela*/
    categoriaComponents.directive('sectionTable', function () {
        return {
            restric: 'E',
            templateUrl: 'proj-html/section-table.html',
            replace: true,
            scope: {
                categorias: '='
            },
            controller: function ($scope, CategoriaAPI) {
                $scope.listandoCategoriasFlag = true;
                $scope.loading = true;
                CategoriaAPI.listar(function (categorias) {
                    $.each(categorias, function (index, cat) {
                        $scope.categorias.push(cat);
                    });
                }, function (erros) {
                    console.log(erros);
                }, function () {
                    $scope.listandoCategoriasFlag = false;
                    $scope.loading = false;
                });

                $scope.removerLinha = function (index) {
                    $scope.categorias.splice(index, 1);
                }
            }

        };
    });
    /* section-table-line -> categoriaTabelaLinha*/
    categoriaComponents.directive('sectionTableLine', function () {
        return {
            restric: 'A',
            templateUrl: 'proj-html/section-table-line.html',
            replace: true,
            scope: {
                categoria: '=',
                deletarCategoriaListener: '&'
            },
            controller: function ($scope, CategoriaAPI) {
                $scope.editandoFlag = false;
                $scope.categoriaParaEdicao = {};
                $scope.loading = false;

                $scope.editarToggle = function () {
                    $scope.editandoFlag = !$scope.editandoFlag;
                    $scope.categoriaParaEdicao.title = $scope.categoria.title;
                    $scope.categoriaParaEdicao.id = $scope.categoria.id;
                };

                $scope.editar = function () {
                    CategoriaAPI.editar($scope.categoriaParaEdicao, function (categoria) {
                        $scope.categoria.title = categoria.title;
                    }, function (erros) {
                        console.log(erros);
                    }, function () {
                        $scope.editandoFlag = false;
                    });
                };

                $scope.deletar = function () {
                    $scope.loading = true;
                    CategoriaAPI.deletar($scope.categoria.id, function () {
                        $scope.deletarCategoriaListener();
                    }, function (erros) {
                        console.log(erros);
                    },function(){
                        $scope.loading = false;
                    });
                };

            }

        };
    });
    /*navbar*/
    categoriaComponents.directive('sectionNavbar', function () {
        return {
            restric: 'A',
            templateUrl: 'proj-html/navbar.html',
            replace: true,
            scope: {}
        };
    });
    /*section-portfolio*/
    categoriaComponents.directive('sectionPortfolio', function () {
        return {
            restric: 'A',
            templateUrl: 'proj-html/section-portfolio.html',
            replace: true,
            scope: {}
        };
    });
    /* section-about */
    categoriaComponents.directive('sectionAbout', function () {
        return {
            restric: 'A',
            templateUrl: 'proj-html/section-about.html',
            replace: true,
            scope: {}
        };
    });
    /* footer */
    categoriaComponents.directive('sectionFooter', function () {
        return {
            restric: 'A',
            templateUrl: 'proj-html/footer.html',
            replace: true,
            scope: {}
        };
    });
    /* modal */
    categoriaComponents.directive('sectionModal', function () {
        return {
            restric: 'A',
            templateUrl: 'proj-html/modal.html',
            replace: true,
            scope: {}
        };
    });
})();