/**
 * Created by heitor on 14/09/15.
 */
$(document).ready(function () {
    /*Jquery*/
    var $formGroups = $('div.form-group');
    var $helpBlocks = $('.help-block');
    var $titleInput = $('#titleInput');
    var $descriptionInput = $('#descriptionInput');
    var $tableItem = $('#tableItem');
    var $addItemForm = $('#addItemForm');
    var $listarAjaxLoader = $('#ajax-loader');
    var $formLock = function (boolean) {
        $("#addItemForm :input").prop("disabled", boolean);
    }

    function limparErros() {
        $formGroups.removeClass('has-error');
        $helpBlocks.text('');
    }

    /*servlet = 'rest'*/
    $.get('rest', function (item) {
        console.log(item);
    }, 'json');

    function adicionarItens(item) {
        var linha = '<tr>';
        linha += '<td>' + item.id + '</td>';
        linha += '<td>' + item.creation + '</td>';
        linha += '<td>' + item.title + '</td>';
        linha += '<td>' + item.description + '</td>';
        linha += '<td>';
        linha += '<button class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i></button>';
        linha += '<img src="img/ajax-loader.gif"  hidden="hidden" class = "ajax-loader" />';
        linha += '</td ></tr>';

        var $linhaObjeto = $(linha);
        var $botao = $linhaObjeto.find('button.btn');
        var $ajaxLoader = $linhaObjeto.find('img');

        $botao.click(function () {
            $botao.hide();
            $ajaxLoader.fadeIn();

            /*servlet = 'delete'*/
            $.post('delete', {'id': item.id})
                .success(
                function () {
                    $linhaObjeto.remove();
                })
                .error(
                function (erros) {
                    alert('Não é possível apagar no momento')
                    $ajaxLoader.hide();
                    $botao.fadeIn();
                });

        });//end click

        $tableItem.append($linhaObjeto);

    }

    function listarItens(itens) {
        $.each(itens, function (i, cat) {
            adicionarItens(cat);
        })
    }

    $listarAjaxLoader.show();

    /*lock input*/
    $formLock(true);

    /*servlet = 'rest'*/
    $.get('rest').success(listarItens).error(function () {
        alert('Não foi possível listar categorias');
    }).always(function () {
        $listarAjaxLoader.fadeOut();
        /*unlock input*/
        $formLock(false);
    });


    function mostrarErros(erros) {
        var helpBlockPrefixo = '#help-block-';
        var formGroupPrefixo = '#form-group-';
        $.each(erros, function (propriedade, valorDaPropriedade) {
            $(helpBlockPrefixo + propriedade).text(valorDaPropriedade);
            $(formGroupPrefixo + propriedade).addClass('has-error');
        });
    }

    $addItemForm.submit(function (evento) {
        evento.preventDefault();
        limparErros();
        var title = $titleInput.val();
        var description = $descriptionInput.val();
        /*lock input*/
        $formLock(true);
        /*servlet = 'new'*/
        var toAdd = {
            "title": title,
            "description": description
        };
        $.post('new', toAdd).success(function (item) {
            adicionarItens(item);
            $titleInput.val('');
            $descriptionInput.val('');
            /*unlock input*/
            $formLock(false);
        }).error(function (erros) {
            mostrarErros(erros.responseJSON);
            /*unlock input*/
            $formLock(false);
        });

    });
});