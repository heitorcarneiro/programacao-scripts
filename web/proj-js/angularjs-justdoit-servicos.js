/**
 * Created by heitor on 08/10/15.
 */
(function () {
    var categoriaServicos = angular.module('categoria-servicos', []);

    categoriaServicos.factory('CategoriaAPI', function ($http) {
        function extrairDados(f) {
            return function (ajaxRetorno) {
                return f(ajaxRetorno.data);
            }
        }

        return {
            deletar: function (id, callbackSucesso, callbackErro, callbackAlways) {
                callbackSucesso = extrairDados(callbackSucesso);
                $http.post('delete', {'id': id}).then(
                    callbackSucesso, function (resposta) {
                        callbackErro(resposta.data)
                    }
                ).finally(callbackAlways);

            },
            editar: function (categoria, callbackSucesso, callbackErro, callbackAlways) {
                callbackSucesso = extrairDados(callbackSucesso);
                $http.post('edit', categoria).then(
                    callbackSucesso, callbackErro).finally(callbackAlways);
            },
            salvar: function (categoria, callbackSucesso, callbackErro, callbackAlways) {
                callbackSucesso = extrairDados(callbackSucesso);
                callbackErro = extrairDados(callbackErro);
                $http.post('new', categoria).then(
                    callbackSucesso, callbackErro).finally(callbackAlways);
            },
            listar: function (callbackSucesso, callbackErro, callbackAlways) {
                callbackSucesso = extrairDados(callbackSucesso);
                $http.get('rest').then(
                    callbackSucesso, callbackErro
                ).finally(callbackAlways);
            }
        };
    });
})();