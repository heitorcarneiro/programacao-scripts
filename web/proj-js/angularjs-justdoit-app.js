/**
 * Created by heitor on 08/10/15.
 */
(function () {
    var categoriaApp = angular.module('categoriaApp', ['categoria-components']);

    categoriaApp.controller('CategoriaCtrl', function ($scope) {
        $scope.categorias = [];

        $scope.adicionarCategoria = function (categoria) {
            $scope.categorias.push(categoria);
        }

    });
})();
