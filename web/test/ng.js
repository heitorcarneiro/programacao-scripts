var nPost = 0;
var getMockVars;
var mockService = function (url, callbackSucesso, callbackErro, callbackAlways){
    nPost++;
    var getVarsHolder = {
        url: url,
        success:function () { return callbackSucesso},
        error: function () { return callbackErro},
        always: function () { return callbackAlways}
    };
    getMockVars = getVarsHolder;
};
angular.module('categoria-servicos', []).factory('CategoriaAPI', function ($http) {
    function extrairDados(f) {
        return function (ajaxRetorno) {
            return f(ajaxRetorno.data);
        }
    }

    return {
        deletar: function (id, callbackSucesso, callbackErro, callbackAlways) {
            callbackSucesso = extrairDados(callbackSucesso);
            $http.post('delete', {'id': id}).then(
                callbackSucesso, function (resposta) {
                    callbackErro(resposta.data)
                }
            ).finally(callbackAlways);

            //mock
            mockService('delete', callbackSucesso, callbackErro, callbackAlways);

        },
        editar: function (categoria, callbackSucesso, callbackErro, callbackAlways) {
            callbackSucesso = extrairDados(callbackSucesso);
            $http.post('edit', categoria).then(
                callbackSucesso, callbackErro).finally(callbackAlways);

            //mock
            mockService('edit', callbackSucesso, callbackErro, callbackAlways);
        },
        salvar: function (categoria, callbackSucesso, callbackErro, callbackAlways) {
            callbackSucesso = extrairDados(callbackSucesso);
            callbackErro = extrairDados(callbackErro);
            $http.post('new', categoria).then(
                callbackSucesso, callbackErro).finally(callbackAlways);

            //mock
            mockService('new', callbackSucesso, callbackErro, callbackAlways);
        },
        listar: function (callbackSucesso, callbackErro, callbackAlways) {
            callbackSucesso = extrairDados(callbackSucesso);
            $http.get('rest').then(
                callbackSucesso, callbackErro
            ).finally(callbackAlways);

            //mock
            mockService('rest', callbackSucesso, callbackErro, callbackAlways);
        }
    };
});

QUnit.test('Controlador Item App', function (assert) {
    angular.module('categoriaApp');

    var injector = angular.injector(['ng', 'categoriaApp']);

    var $controller = injector.get('$controller');
    var $categoriaScope = {};
    $controller('CategoriaCtrl',
        {$scope: $categoriaScope});

    assert.equal(0, $categoriaScope.categorias.length, 'Atributos Iniciais:Não deveria ter elemento inicialmente')

    categoria = {id: 1, title: 'QUnit', description: "Iniciando testes"};
    $categoriaScope.adicionarCategoria(categoria);


    assert.equal(1, $categoriaScope.categorias.length, 'Adicionar Categoria:Deveria ter um elemnto após adição');
    assert.equal(categoria, $categoriaScope.categorias[0], 'Adicionar Categoria:Elemento deveria ser igual ao adicionado')


});

QUnit.test('Controlador componentes: Register', function (assert) {
    angular.module('categoria-components');

    var injector = angular.injector(['ng', 'categoria-components']);

    injector.invoke(function ($compile, $rootScope, $templateCache) {
        $templateCache.put('proj-html/section-register.html', '<a>Qualquer</a>')
        var el = angular.element('<section-register></section-register>');
        el = $compile(el)($rootScope);
        $rootScope.$digest();
        var isolatedScope = el.isolateScope();
        var scope = el.scope();
        /**
         * Test
         */
            // atributos
        assert.equal('QunitTest', isolatedScope.categoria.title, 'Atributos Iniciais:Deveria aparecer o titulo: QunitTest');
        assert.equal(false, isolatedScope.salvandoCategoriaFlag, 'Atributos Iniciais:Nao deveria aparecer o flag de salvar atividades');
        assert.equal(false, isolatedScope.loading, 'Atributos Iniciais:Nao deveria aparecer o ajax loader');
        // salvar
        isolatedScope.categoria = {title: 'QunitTest', description: 'Testando funcao salvar'};
        assert.equal('Testando funcao salvar', isolatedScope.categoria.description, 'Salvar:Deveria aparecer o titulo: Testando funcao salvar');
        isolatedScope.salvar();
        assert.strictEqual(1, nPost, 'Verificando que get é chamado apenas uma vez');
        assert.strictEqual('new', getMockVars.url, 'Verificando url de chamada ajax para listar categorias');
        assert.equal(true, isolatedScope.salvandoCategoriaFlag, 'Salvar:O flag de salvar atividades deveria aparecer');
        assert.equal(true, isolatedScope.loading, 'Salvar:O ajax loader deveria aparecer enquanto o item e salvo');
        getMockVars.always();
        assert.equal(true, isolatedScope.salvandoCategoriaFlag, 'Salvar:O flag de salvar atividades deveria aparecer');
        assert.equal(true, isolatedScope.loading, 'Salvar:O ajax loader deveria aparecer enquanto o item e salvo');
    });
});

QUnit.test('Controlador componentes: Table', function (assert) {
    angular.module('categoria-components');

    var injector = angular.injector(['ng', 'categoria-components']);

    injector.invoke(function ($compile, $rootScope, $templateCache) {
        $templateCache.put('proj-html/section-table.html', '<a>Qualquer</a>')
        var el = angular.element('<section-table></section-table>');
        el = $compile(el)($rootScope);
        $rootScope.$digest();
        var isolatedScope = el.isolateScope();
        var scope = el.scope();
        /**
         * Test
         */
            // atributos
        isolatedScope.categorias = [];
        assert.equal(true, isolatedScope.listandoCategoriasFlag, 'Listar:Ao iniciar deveria aparecer flag Carregando Tabela');
        assert.equal(true, isolatedScope.loading, 'Listar:O ajax loader deve aparecer enquanto a tabela e carregada');

    });
});

QUnit.test('Controlador componentes: TableLine', function (assert) {
    angular.module('categoria-components');

    var injector = angular.injector(['ng', 'categoria-components']);

    injector.invoke(function ($compile, $rootScope, $templateCache) {
        $templateCache.put('proj-html/section-table-line.html', '<a>Qualquer</a>')
        var el = angular.element('<section-table-line></section-table-line>');
        el = $compile(el)($rootScope);
        $rootScope.$digest();
        var isolatedScope = el.isolateScope();
        var scope = el.scope();
        /**
         * Test
         */
            // atributos
        assert.equal(false, isolatedScope.editandoFlag, 'Atributos Iniciais:O flag de edicao nao deve aparecer');
        assert.equal(false, isolatedScope.loading, 'Atributos Iniciais:O ajax loader nao deve aparecer');
        // editarToggle
        isolatedScope.categoria = {title: 'QunitTest', description: 'Testando funcao editarToggle'};
        isolatedScope.editarToggle();
        assert.equal(true, isolatedScope.editandoFlag, 'Editar Toggle:O flag de edicao aparece, efeito visual tabela');
        assert.equal(false, isolatedScope.loading, 'Editar Toggle:O ajax loader nao deve aparecer');
        // editar
        isolatedScope.editar();
        assert.equal(true, isolatedScope.editandoFlag, 'Editar:O flag de edicao aparece, esperando confirmacao do servidor');
        assert.equal(false, isolatedScope.loading, 'Editar:O ajax loader nao deve aparecer');
        // deletar
        isolatedScope.deletar();
        assert.equal(true, isolatedScope.editandoFlag, 'Deletar:O flag de edicao aparece, efeito visual tabela');
        assert.equal(true, isolatedScope.loading, 'Deletar:O ajax loader aguarda callback do servidor para excluir');
    });
});