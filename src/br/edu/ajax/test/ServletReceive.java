package br.edu.ajax.test;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by heitor on 23/09/15.
 */
@WebServlet("/receive")
public class ServletReceive extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader reader = request.getReader();
        StringBuilder inputStringBuilder = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            inputStringBuilder.append(line);
        }

        response.setContentType("application/json");
        String s = inputStringBuilder.toString();

        Object obj = getObject(s);

    }

    private void outputPage(PrintStream out, String result) {
        out.println(getObject(result));
    }
    protected Object getObject(String parameterName){
        return new Gson().fromJson(parameterName, Object.class);
    }
}
