package br.edu.ajax.test;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by heitor on 23/09/15.
 */
@WebServlet("/send")
public class ServletSend extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String parameterName = "TEST";
        response.setContentType("application/json");
        outputPage(new PrintStream(response.getOutputStream()), parameterName);

    }

    private void outputPage(PrintStream out, String result) {
        out.println(getStuff(result));
    }

    private Object getStuff(String s) {
        return new Gson().fromJson(s, Object.class);
    }

}
