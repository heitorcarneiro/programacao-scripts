package br.edu.controller;

import br.edu.scripts.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by heitor on 22/09/15.
 */
@WebServlet("/new")
public class ServletNew extends HttpServlet {
    private final static int DELAY = 1000;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {Thread.sleep(DELAY);} catch (InterruptedException e) {e.printStackTrace();}
        String json = Json.requestJson(request);
        Item toAdd = Json.fromRequestJson(json);
        if (ValidInformation.valid(toAdd)) {
            DataBase.getInstance().addItens(toAdd);
            String answerToPage = Json.toJson(toAdd);
            Callback.onSuccess(response, answerToPage);
        } else {
            Callback.onError(response);
        }
    }

}
