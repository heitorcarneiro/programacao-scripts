package br.edu.controller;

import br.edu.scripts.Item;
import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.List;

public class Json {
    public static String requestJson(HttpServletRequest request) {
        StringBuffer sb = new StringBuffer();
        try {
            BufferedReader reader = request.getReader();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static Item fromRequestJson(String json) {
        Item newItem = null;
        JsonParse parse = new Gson().fromJson(json, JsonParse.class);
        if (parse.getTitle() != null && parse.getDescription() != null) {
            newItem = new Item(parse.getTitle(), parse.getDescription());
        }
        return newItem;
    }

    public static Long fromRequestJsonId(String json) {
        Long result = null;
        JsonParse parse = new Gson().fromJson(json, JsonParse.class);
        if (parse.getId() != null) {
            result = parse.getId();
        }
        return result;
    }

    public static JsonParse fromRequestJsonIdTitle(String json) {
        JsonParse parse = new Gson().fromJson(json, JsonParse.class);
        if (parse.getTitle() != null && parse.getId() != null) {
            return parse;
        }
        return null;
    }

    public static String toJson(Item item) {
        String json = new Gson().toJson(item);
        return json;
    }

    public static Item fromJson(String json) {
        Item newItem = new Gson().fromJson(json, Item.class);
        return newItem;
    }

    public static String toJson(List<Item> item) {
        String json = new Gson().toJson(item);
        return json;
    }

    public static List<Item> fromJsonList(String json) {
        List<Item> newItem = new Gson().fromJson(json, List.class);
        return newItem;
    }

}
