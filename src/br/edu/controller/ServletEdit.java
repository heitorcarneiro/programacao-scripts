package br.edu.controller;

import br.edu.scripts.DataBase;
import br.edu.scripts.Item;
import br.edu.scripts.ValidInformation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.Data;
import java.io.IOException;

/**
 * Created by heitor on 08/10/15.
 */
@WebServlet("/edit")
public class ServletEdit extends HttpServlet {
    private final static int DELAY = 1000;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {Thread.sleep(DELAY);} catch (InterruptedException e) {e.printStackTrace();}
        String json = Json.requestJson(request);
        System.out.println(json);
        JsonParse temp = Json.fromRequestJsonIdTitle(json);
        if (ValidInformation.valid(temp)) {
            Item toEditItem = DataBase.getInstance().searchItem(temp.getId());
            if (!temp.getTitle().equals(toEditItem.getTitle())) {
                toEditItem.setTitle(temp.getTitle());
                DataBase.getInstance().updateItem(toEditItem);
            }
            Callback.onSuccess(response, Json.toJson(toEditItem));
        } else {
            Callback.onError(response);
        }
    }
}
