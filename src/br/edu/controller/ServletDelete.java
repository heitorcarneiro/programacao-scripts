package br.edu.controller;

import br.edu.scripts.DataBase;
import br.edu.scripts.ValidInformation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by heitor on 22/09/15.
 */
@WebServlet("/delete")
public class ServletDelete extends HttpServlet {
    private final static int DELAY = 1000;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {Thread.sleep(DELAY);} catch (InterruptedException e) {e.printStackTrace();}
        String json = Json.requestJson(request);
        Long id = Json.fromRequestJsonId(json);
        if (ValidInformation.valid(id)) {
            /*remove from server*/
            DataBase.getInstance().removeItem(id);
            Callback.onSuccess(response);
        } else {
            Callback.onError(response);
        }
    }

}
