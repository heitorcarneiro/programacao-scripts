package br.edu.controller;

/**
 * Created by heitor on 08/10/15.
 */
public class JsonParse {
    private Long id;
    private String title;
    private String description;

    public JsonParse(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public JsonParse(Long id) {
        this.id = id;
    }

    public JsonParse(Long id, String title) {
        this.id = id;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
