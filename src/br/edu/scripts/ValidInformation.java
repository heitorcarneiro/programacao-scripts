package br.edu.scripts;

/**
 * Created by heitor on 24/09/15.
 */

import java.util.List;

public class ValidInformation {
    public static boolean valid(String parameter) {
        if (parameter != null && !parameter.isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean valid(List<String> parameters) {
        boolean result = true;
        for (String list : parameters) {
            result = result && (valid(list));
        }
        return result;
    }

    public static boolean valid(Object parameter) {
        if (parameter != null) {
            return true;
        }
        return false;
    }

    public static boolean validNumber(String number) {
        boolean result = false;
        if (valid(number)) {
            try {
                Long n = Long.parseLong(number);
                result = true;
            } catch (Exception e) {
            }
        }
        return result;
    }


}
