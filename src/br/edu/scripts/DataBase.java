package br.edu.scripts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by heitor on 22/09/15.
 */
public class DataBase {
    private static DataBase ourInstance = new DataBase();
    private List<Item> itens;

    public static DataBase getInstance() {
        return ourInstance;
    }

    private DataBase() {
        itens = new ArrayList<>();
        /*Dump*/
        itens.addAll(Dump.getItens());

    }

    public void addItens(long id, String title, String description) {
        itens.add(new Item(id, title, description));
    }

    public void addItens(Item item) {
        if (item != null)
            itens.add(item);
    }

    public void removeItem(Item item) {
        if (item != null)
            itens.remove(item);
    }

    public void removeItem(Long id) {
        Item item = searchItem(id);
        if (item != null) {
            itens.remove(item);
        }
    }

    public void updateItem(Item item) {
        for (int i = 0; i < itens.size(); i++) {
            if (itens.get(i).getId().equals(item.getId())) {
                itens.set(i, item);
            }
        }
    }

    public Item searchItem(Long id) {
        Item result = null;
        for (Item item : itens) {
            if (item.getId().equals(id)) {
                result = item;
            }
        }
        return result;
    }

    public List<Item> searchItem(String title) {
        List<Item> result = new ArrayList<>();
        for (Item item : itens) {
            if (item.getTitle().equals(title)) {
                result.add(item);
            }
        }
        return result;
    }

    public List<Item> getItens() {
        return itens;
    }


    public int getItensSize() {
        return itens.size();
    }

}
