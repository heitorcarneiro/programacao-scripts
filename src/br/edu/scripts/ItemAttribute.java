package br.edu.scripts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by heitor on 24/09/15.
 */
public class ItemAttribute {
    private static final long LOWER_RANGE = 1000000; //assign lower range value
    private static final long UPPER_RANGE = 9999999; //assign upper range value
    private static final String FORMAT = "dd/M/yyyy hh:mm:ss";

    public static Long generateId() {
        Random random = new Random();
        return LOWER_RANGE +
                (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
    }

    public static Long random(){
        Random random = new Random();
        return LOWER_RANGE +
                (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
    }

    public static String creationDate(){
        return new SimpleDateFormat(FORMAT)
                .format(new Date());
    }
}
