package br.edu.scripts;

public class Item {
    private Long id;
    private String title;
    private String creation;
    private String description;

    public Item(String title, String description) {
        /*Generate fake ID*/
        this.id = ItemAttribute.generateId();
        this.title = title;
        this.creation = ItemAttribute.creationDate();
        this.description = description;
    }

    public Item(Long id, String title, String description) {
        this.id = id;
        this.title = title;
        this.creation = ItemAttribute.creationDate();
        this.description = description;
    }


    public Item(Long id, String title, String creation, String description) {
        this.id = id;
        this.title = title;
        this.creation = creation;
        this.description = description;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreation() {
        return creation;
    }

    public void setCreation(String creation) {
        this.creation = creation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
