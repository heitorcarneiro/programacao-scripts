package br.edu.scripts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by heitor on 22/09/15.
 */
public class Dump {
    public static List<Item> getItens() {
        List<Item> itens = Arrays.asList(
                new Item("Entrega 5 - Programação de Scripts", "Na entrega 5 vcs deverão implementar o CRUD visual, " + "no contexto da aplicação de vcs, utilizando JQuery."),
                new Item("Tarefa de MongoDB", "Week 6 MongoDB University M101p "),
                new Item("Tarefa de Segurança da Informação", "Análise de ameaças utilizando as classes de controles do NIST")
        );
        return itens;
    }
}
