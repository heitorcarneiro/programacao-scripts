package br.edu.test;

import br.edu.scripts.DataBase;
import br.edu.scripts.Item;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by heitor on 23/09/15.
 */
public class DataBaseTest {
    private static DataBase data = DataBase.getInstance();
    private final long ID = 10000000;
    private Item item = new Item(ID, "titulo", "ola mundo");

    @Test
    public void AddItemTest() {
        data.addItens(item);
        Integer size = data.getItensSize();
        Integer expected = 4;

        assertEquals(expected, size);
    }

    @Test
    public void removeItemIdTest() {
        data.removeItem(ID);

        Integer actual = data.getItensSize();
        Integer expected = 3;

        assertEquals(expected, actual);
    }


    @Test
    public void removeItemTest() {
        data.removeItem(item);

        Integer actual = data.getItensSize();
        Integer expected = 3;

        assertEquals(expected, actual);
    }
}